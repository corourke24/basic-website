// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .

/* mobile header */

function openNav() {
    document.getElementById("myNav").style.height = "100%";
}

function closeNav() {
    document.getElementById("myNav").style.height = "0%";
}

/* Timer */

/* Set date to countdown to */
var countDownDate = new Date("Jan 5, 2019 15:37:25").getTime();

/* update every 1 sec */
var x = setInterval(function() {
   
   /* today's date/time */
   var now = new Date().getTime();
   
   var distance = countDownDate - now;
   
   /* time caculations(d,h,m,s) */
   var days = Math.floor(distance / (1000 * 60 * 60 * 24));
   var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
   var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
   var seconds = Math.floor((distance % (1000 * 60)) / 1000);
   
   /* display countdown time in element with id="timer" */
   document.getElementById("timer").innerHTML = days + "d " + hours + "h " + minutes + "m " +seconds + "s ";
   
   /* display message when tiemr ends */
   if (distance < 0) {
       clearInterval(x);
       document.getElementById("timer").innerHTML = "Countdown Over";
   }
});